import csv
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
#from preprocess import add_extra_to_dict

def read_glove_vecs(glove_file="./glove.6B.50d.txt"):
    with open(glove_file, 'r', encoding = 'utf8') as f:
        words = set()
        word_to_vec_map = {}
        for line in f:
            line = line.strip().split()
            curr_word = line[0]
            words.add(curr_word)
            word_to_vec_map[curr_word] = np.array(line[1:], dtype=np.float64)
        
        i = 0
        words_to_index = {}
        index_to_words = {}
        for w in sorted(words):
            words_to_index[w] = i
            index_to_words[i] = w
            i = i + 1
    return words_to_index, index_to_words, word_to_vec_map

def read_csv(filename = 'data/data_10.csv'):
    ans = []
    ques = []

    with open (filename) as csvDataFile:
        csvReader = csv.reader(csvDataFile)

        for row in csvReader:
            ans.append(row[0])
            ques.append(row[1])
    #print(type(ans))
    X = np.asarray(ans)
    #print(type(a))
    Y = np.asarray(ques)

    return X, Y

def map_dict_to_list(iw, wv):
    emb = []
    for idx in range(0,len(iw)):
        emb.append(wv[iw[idx]])
    return emb
        

'''      
def plot_confusion_matrix(y_actu, y_pred, title='Confusion matrix', cmap=plt.cm.gray_r):
    
    df_confusion = pd.crosstab(y_actu, y_pred.reshape(y_pred.shape[0],), rownames=['Actual'], colnames=['Predicted'], margins=True)
    
    df_conf_norm = df_confusion / df_confusion.sum(axis=1)
    
    plt.matshow(df_confusion, cmap=cmap) # imshow
    #plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(df_confusion.columns))
    plt.xticks(tick_marks, df_confusion.columns, rotation=45)
    plt.yticks(tick_marks, df_confusion.index)
    #plt.tight_layout()
    plt.ylabel(df_confusion.index.name)
    plt.xlabel(df_confusion.columns.name)
'''


'''
#Testing all
#Remember importing from other file
#read_csv()
wi,iw,wv = read_glove_vecs("./glove.6B.50d.txt")
add_extra_to_dict(wi,iw,wv)
emb = map_dict_to_list(iw,wv)
print(emb[0])
'''
